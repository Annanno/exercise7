package com.example.exercise7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.example.exercise7.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }


    private fun init(){
        binding.signIn.setOnClickListener {
            val email = binding.emailET.text
            if (email!!.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(this, "success", Toast.LENGTH_SHORT).show()
            }else Toast.makeText(this, "email is not valid", Toast.LENGTH_SHORT).show()
        }
    }


}